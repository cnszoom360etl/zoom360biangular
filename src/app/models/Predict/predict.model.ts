export class SavePredictionModel
    {
        userID  :string;
        workspaceID  :string;
        clientId  :string;
        scriptName  :string;
        AmountSpentUSD  :string;
        results  :string;
        impressions  :string;
        purchaseConversionValueUSD  :string;
        reach  :string;
        resultTypeCode  :string;
        purchaseROASReturnonAdSpend  :string;
        costPerResultUSD  :string;
        Status  :string;
        PredictedResult  :string;
        
    }