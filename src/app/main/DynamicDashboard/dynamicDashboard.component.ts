import { Component, OnInit, ViewChild } from '@angular/core';
import { GetWorkbookdto } from 'src/app/models/DynamicDashboard/Workbookdto';
import { AppMenuService } from '../../Services/common/app-menu.service';
import { WorkbookpagesComponent } from './workbookpages/workbookpages.component';


@Component({
  selector: 'app-publish',
  templateUrl: './dynamicDashboard.component.html',
  styleUrls: ['./dynamicDashboard.component.css']
})
export class DynamicDashboardComponent implements OnInit {
  @ViewChild("test",{static:true}) test: WorkbookpagesComponent;
  mainmenuID:number=7;
  Getworkbookdtos:GetWorkbookdto[]=[];

  constructor(public MenuService: AppMenuService) { }

  ngOnInit(): void {
    this.MenuService.getsubMenuSection(this.mainmenuID);
    this.MenuService.getWorkbooks().subscribe(res => {
      debugger
      this.Getworkbookdtos = res
      
    })
  }


  getpagewidgets(test1){
        this.test.page = test1
        this.test.getWidgets(test1);
        // const slides = document.getElementsByClassName('active-class');
        // for (let i = 0; i < slides.length; i++) {
        //     const slide = slides[i] as HTMLElement;
        //     slide.classList.remove('active-class');
        //     slide.className='css-1wfdhj';
        // }
        // let ele = document.getElementById("link"+test1.id) as HTMLElement;
        // ele.className = 'active-class';
        this.MenuService.setActiveClass("link"+test1.id);

  }
}

