import { Component, OnInit } from '@angular/core';
import { PredictionService } from 'src/app/services/prediction-service.service';

@Component({
  selector: 'app-predictionPCV',
  templateUrl: './predictionPCV.component.html',
  styleUrls: ['./predictionPCV.component.css']
})
export class PredictionPCVComponent implements OnInit {
  purchaseRoas:number=0;
  costPerResults:number=0;
  impression:number=0;
  amountSpent:number=0;
  reach:number=0;
  pridictedPCV:string;
  constructor(public predictionService :PredictionService) { }

  ngOnInit() {
  }
  getPCV(){
//  var reach= {
//     model_type:"REACH",
//     data: [
//         {
//           "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
//           "RESULTS": this.Results,
//           "IMPRESSIONS": this.impression}
//           ]
//     }
    this.predictionService.getPCV({
      model_type:"PCV",
      data: [
          {
            "PURCHASE_ROAS_(RETURN_ON_AD_SPEND)": this.purchaseRoas,
            "IMPRESSIONS": this.impression,
            "COST_PER_RESULT_(USD)": this.costPerResults,
            "AMOUNT_SPENT_(USD)":this.amountSpent,
            "REACH": this.reach,
          }
            ]
      }).subscribe((data:any)=>{
        debugger
        this.pridictedPCV=data.PredictedPCV;
      
      });
}

}
