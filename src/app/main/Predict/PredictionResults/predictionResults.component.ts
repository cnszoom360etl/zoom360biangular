import { Component, OnInit } from '@angular/core';
import { PredictionService } from 'src/app/services/prediction-service.service';

@Component({
  selector: 'app-predictionResults',
  templateUrl:'./predictionResults.component.html',
  styleUrls: ['./predictionResults.component.css']
})
export class predictionResultsComponent implements OnInit {
  amountSpentUSD:number=0;
  reach:number=0;
  impression:number=0;
  costPerResult:number=0;
  pridictedRsults:string;
  constructor(public predictionService :PredictionService) { }

  ngOnInit() {
  }
getResults(){
//  var reach= {
//     model_type:"REACH",
//     data: [
//         {
//           "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
//           "RESULTS": this.Results,
//           "IMPRESSIONS": this.impression}
//           ]
//     }
    this.predictionService.getResults({
      model_type:"RESULTS",
      data: [
          {
            "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
            "REACH": this.reach,
            "IMPRESSIONS": this.impression,
            "COST_PER_RESULT_":this.costPerResult
          }
            ]
      }).subscribe((data:any)=>{
        debugger
        this.pridictedRsults=data.PredictedRESULTS;
      
      });
}

}
