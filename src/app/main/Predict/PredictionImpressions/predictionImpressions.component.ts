import { Component, OnInit } from '@angular/core';
import { PredictionService } from 'src/app/services/prediction-service.service';

@Component({
  selector: 'app-predictionImpressions',
  templateUrl: './predictionImpressions.component.html',
  styleUrls: ['./predictionImpressions.component.css']
})
export class predictionImpressionsComponent implements OnInit {
  amountSpentUSD:number=0;
  Results:number=0;
  reach:number=0;
  pridictedImpression:string;
  constructor(public predictionService :PredictionService) { }

  ngOnInit() {
  }
  getImpressions(){
//  var reach= {
//     model_type:"REACH",
//     data: [
//         {
//           "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
//           "RESULTS": this.Results,
//           "IMPRESSIONS": this.impression}
//           ]
//     }
    this.predictionService.getImpressions({
      model_type:"IMPRESSION",
      data: [
          {
            "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
            "RESULTS": this.Results,
            "REACH": this.reach}
            ]
      }).subscribe((data:any)=>{
        debugger
        this.pridictedImpression=data.PredictedIMPRESSION;
      
      });
}

}
