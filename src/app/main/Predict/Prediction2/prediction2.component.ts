import { Component, OnInit } from '@angular/core';
import { PredictionService } from 'src/app/services/prediction-service.service';

@Component({
  selector: 'app-prediction2',
  templateUrl: './prediction2.component.html',
  styleUrls: ['./prediction2.component.css']
})
export class Prediction2Component implements OnInit {
  purchaseConversionValue:number=0;
  impression:number=0;
  result:number=0;
  reach:number=0;
  resultTypeCode:number=0;
  PredictedAmountSpend:string;
  constructor(public predictionService :PredictionService) { }

  ngOnInit() {
  }
  getAmountSpend(){
//  var reach= {
//     model_type:"REACH",
//     data: [
//         {
//           "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
//           "RESULTS": this.Results,
//           "IMPRESSIONS": this.impression}
//           ]
//     }
    this.predictionService.getAmountSpend(
      {
      model_type:"AMOUNT SPEND",
      data: [
          {
            "PURCHASES_CONVERSION_VALUE_(USD)": this.purchaseConversionValue,
            "IMPRESSIONS": this.impression,
            "RESULTS":this.result,
            "REACH": this.reach,
            "RESULT_TYPE_CODE": this.resultTypeCode,
          }
            ]
      }).subscribe((data:any)=>{
        debugger
        this.PredictedAmountSpend=data["PredictedAMOUNT SPEND"];
      
      });
}

}
