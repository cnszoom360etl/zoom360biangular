import { Component, OnInit } from '@angular/core';
import { PredictionService } from 'src/app/services/prediction-service.service';

@Component({
  selector: 'app-prediction1',
  templateUrl: './prediction1.component.html',
  styleUrls: ['./prediction1.component.css']
})
export class Prediction1Component implements OnInit {
  amountSpentUSD:number=0;
  Results:number=0;
  impression:number=0;
  pridictedReach:string;
  constructor(public predictionService :PredictionService) { }

  ngOnInit() {
  }
getReach(){
//  var reach= {
//     model_type:"REACH",
//     data: [
//         {
//           "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
//           "RESULTS": this.Results,
//           "IMPRESSIONS": this.impression}
//           ]
//     }
    this.predictionService.getReach({
      model_type:"REACH",
      data: [
          {
            "AMOUNT_SPENT_(USD)": this.amountSpentUSD,
            "RESULTS": this.Results,
            "IMPRESSIONS": this.impression}
            ]
      }).subscribe((data:any)=>{
        debugger
        this.pridictedReach=data.PredictedREACH;
      
      });
}

}
