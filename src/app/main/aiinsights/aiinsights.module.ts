import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyzeRoutingModule } from './aiinsights-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';
import { KpiGraphComponent } from '../widget/kpi-graph/kpi-graph.component';
import { AIinsightsComponent } from './aiinsights.component';
// Import FusionCharts library and chart modules

import { WidgetModule } from '../widget/widget.module';
import { AIinsightWidgetComponent } from './aiinsight-widget/aiinsight-widget.component';
import { ButtonModule, CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { AgGridDataViewerComponent } from './ag-grid-data-viewer/ag-grid-data-viewer.component';



@NgModule({
  declarations: [
    AIinsightsComponent,
    KpiGraphComponent,
    AIinsightWidgetComponent,
  
  ],
  imports: [
    SharedModule,
    CommonModule,
    AnalyzeRoutingModule,
    WidgetModule,
    ButtonModule,
    CheckBoxModule,
   
  ]
})
export class aiinsightsModule {
  
 }
